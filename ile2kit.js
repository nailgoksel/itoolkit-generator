// =========================
// global constants
// ==========================
var ILEVARCNT = 0;
var ILEVAR=new Array;
function ile_var_clear() {
  ILEVAR=new Array();
  ILEVARCNT = 0;
}
function ile_clean_name(pn) {
  var tp = pn.trim();
  if (tp < 1) {
    return 'MISSING';
  }
  var c = tp.charAt(tp.length - 1);
  if (c == '.') {
    tp = tp.substr(0,tp.length - 1);
  }
  return tp;
} 
function ile_unique_name(n) {
  var look = ile_clean_name(n);
  var find = false;
  for (var i =0; i < ILEVAR.length; i++) {
    var v = ILEVAR[i];
    if (look == v) {
      ILEVARCNT++;
      find = look + ILEVARCNT.toString();
      ILEVAR.push(find);
      break;
    }
  }
  if (!find) {
    find = look;
    ILEVAR.push(find);
  }
  return find;
}

var ILEPGM="*PGM";
var ILELIB="*LIBL";
var ILECONST=new Array;
function ile_const_clear() {
  ILECONST=new Array();
}
function ile_const_push(n,v) {
  ILECONST.push(Array(n.trim(),v.trim()));
}
function ile_const_value(n) {
  var look = n.trim();
  for (var i =0; i < ILECONST.length; i++) {
    var h = ILECONST[i][0];
    var v = ILECONST[i][1];
    if (look == h) {
      return v;
    }
  }
  return n;
}


// =========================
// php
// https://github.com/zendtech/IbmiToolkit
// ==========================
var PHPFORMAT = "0";

function php_newline(line) {
  return line + "\n";
}

function php_var_null(phpVar) {
  var php = '';
  var s = phpVar.split('[');
  if (s.length > 1) {
    php += s[0] + ' = null;' 
  } else {
    php += phpVar + ' = null;' 
  }
  return php_newline(php);
}

/**
 * @param $type
 * @param $io
 * @param string $comment
 * @param string $varName
 * @param $value
 * @param string $varying
 * @param int $dimension
 * @param string $by
 * @param bool $isArray
 * @param null $labelSetLen
 * @param null $labelLen
 * @param string $ccsidBefore
 * @param string $ccsidAfter
 * @param bool $useHex
 * @throws \Exception
 * __construct( $type,  $io, $comment='', $varName = '', $value, $varying = 'off', 
 *              $dimension = 0, $by = 'ref', $isArray = false, 
 *              $labelSetLen = null, $labelLen = null,
 *              $ccsidBefore = '', $ccsidAfter = '', $useHex = false)
 */
function php_toolkit_ProgramParameter(phpVar,
  type,  io, comment, varName, value, 
  varying, dim, by, 
  isArray, labelSetLen, labelLen,
  ccsidBefore, ccsidAfter, useHex) 
{
  var php = '';
  var p = 0;
  type = type.trim();
  if (type.length > 0) {
    p = type.indexOf('unsupported');
    if (p > -1) {
      php += '// *** error ';
    }
  } else {
    php += '// *** error ';
  }
  php += phpVar + ' = new ProgramParameter('
  php += '"'+type+'"';
  if (io) 
    php += ', "'+io+'"';
  else
    php += ', "both"'
  if (comment) 
    php += ', "'+comment+'"';
  else
    php += ', ""';
  if (varName) 
    php += ', "'+varName+'"';
  else
    php += ', ""';
  if (value.length > 0 && ! isNaN(value) ) {
    value = value.toString();
    php += ', ' + value;
  } else {
    php += ', "'+value+'"';
  }
  if (varying) 
    php += ', "'+varying+'"';
  else
    php += ', "off"';
  if (dim) 
    php += ', '+dim;
  else
    php += ', 0';
  if (by) 
    php += ', "'+by+'"';
  else
    php += ', "ref"';
  if (isArray)
    php += ', true';
  else
    php += ', false';
  if (labelSetLen)
    php += ', "'+labelSetLen+'"';
  else
    php += ', null';
  if (labelLen)
    php += ', "'+labelLen+'"';
  else
    php += ', null';
  if (ccsidBefore)
    php += ', "'+ccsidBefore+'"';
  else
    php += ', ""';
  if (ccsidAfter)
    php += ', "'+ccsidAfter+'"';
  else
    php += ', ""';
  if (useHex)
    php += ', true';
  else
    php += ', false';
  php += ');';
  return php_newline(php);
}
/*
function AddParameterChar($io, $size , $comment, $varName = '', $value= '', $varying = 'off',$dimension = 0,
                          $by='', $isArray = false, $ccsidBefore = '', $ccsidAfter = '', $useHex = false)
function AddParameterInt32($io, $comment, $varName = '', $value = '', $dimension = 0)
function AddParameterSize($comment, $varName = '', $labelFindLen)
function AddParameterSizePack($comment, $varName = '', $labelFindLen)
function AddParameterInt8($io, $comment, $varName='', $value='', $dimension=0)
function AddParameterInt16($io, $comment, $varName='', $value='', $dimension=0)
function AddParameterInt64($io, $comment, $varName = '', $value = '', $dimension = 0)
function AddParameterUInt8($io, $comment, $varName='', $value='', $dimension=0)
function AddParameterUInt16($io, $comment, $varName='', $value='', $dimension=0)
function AddParameterUInt32($io, $comment, $varName = '', $value = '', $dimension =0)
function AddParameterUInt64($io, $comment, $varName = '', $value='', $dimension=0)
function AddParameterFloat($io, $comment, $varName = '', $value='', $dimension=0)
function AddParameterReal($io, $comment, $varName = '', $value='', $dimension=0)
function AddParameterPackDec($io, $length ,$scale , $comment, $varName = '', $value='', $dimension=0)
function AddParameterZoned($io, $length ,$scale , $comment, $varName = '', $value='', $dimension=0)
function AddParameterHole($size , $comment='hole')
function AddParameterBin($io, $size , $comment, $varName = '', $value='',$dimension =0)

function AddParameter($type, $io, $comment, $varName = '', $value = '', $varying = 'off', $dimension = 0)

function AddParameterArray($array)
  self::AddParameter($element['type'],
                     $element['io'],
                     $element['comment'],
                     $element['var'],
                     $element['data'],
                     $element['varying'],
                     $element['dim']);

function AddDataStruct(array $parameters, $name='struct_name', $dim=0, $by='', $isArray=false, $labelLen = null, $comment = '', $io = 'both')
*/
function php_toolkit_ProgramParameter2(phpVar,
  rLen, rType, rPre,
  io, comment, varName, value, 
  varying, dim, by) 
{
  if (PHPFORMAT == "0") {
    return php_toolkit_ProgramParameter(phpVar,
      rLen+rType+rPre,  io, comment, varName, value, 
      varying, dim, by, 
      false, null, null,
      null, null, false);
  }
  if (!varying) {
    varying="off";
  }
  if (!dim) {
    dim="0";
  }
  if (!by) {
    by="";
  }
  switch (rType) {
  case 'i':
    switch(rLen) {
    case '3':
      return phpVar + ' = ' + '$tk->AddParameterInt8("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    case '5':
      return phpVar + ' = ' + '$tk->AddParameterInt16("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    case '10':
      return phpVar + ' = ' + '$tk->AddParameterInt32("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    case '20':
      return phpVar + ' = ' + '$tk->AddParameterInt64("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    default:
      return phpVar + ' = ' + '$tk->AddParameterInt32("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    }
    break;
  case 'u':
    switch(rLen) {
    case '3':
      return phpVar + ' = ' + '$tk->AddParameterUInt8("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    case '5':
      return phpVar + ' = ' + '$tk->AddParameterUInt16("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    case '10':
      return phpVar + ' = ' + '$tk->AddParameterUInt32("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    case '20':
      return phpVar + ' = ' + '$tk->AddParameterUInt64("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    default:
      return phpVar + ' = ' + '$tk->AddParameterUInt32("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    }
    break;
  case 'f':
    switch(rLen) {
    case '4':
      return phpVar + ' = ' + '$tk->AddParameterFloat("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    case '8':
      return phpVar + ' = ' + '$tk->AddParameterReal("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    default:
      return phpVar + ' = ' + '$tk->AddParameterReal("'+io+'", "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
    }
    break;
  case 'p':
    return phpVar + ' = ' + '$tk->AddParameterPackDec("'+io+'", '+rLen+', '+rPre+', "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
  case 's':
    return phpVar + ' = ' + '$tk->AddParameterZoned("'+io+'", '+rLen+', '+rPre+', "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
  case 'b':
    return phpVar + ' = ' + '$tk->AddParameterBin("'+io+'", '+rLen+', "'+comment+'", "'+varName+'", '+value+', '+dim+');'+"\n";
  case 'a':
    return phpVar + ' = ' + '$tk->AddParameterChar("'+io+'", '+rLen+', "'+comment+'", "'+varName+'", "'+value+'", "'+varying+'", '+dim+', "'+by+'");'+"\n";
  case 'd':
    return phpVar + ' = ' + '$tk->AddParameterChar("'+io+'", 10, "'+comment+'", "'+varName+'", "'+value+'", "'+varying+'", '+dim+', "'+by+'");'+"\n";
  case 't':
    return phpVar + ' = ' + '$tk->AddParameterChar("'+io+'", 8, "'+comment+'", "'+varName+'", "'+value+'", "'+varying+'", '+dim+', "'+by+'");'+"\n";
  case 'z':
    return phpVar + ' = ' + '$tk->AddParameterChar("'+io+'", 26, "'+comment+'", "'+varName+'", "'+value+'", "'+varying+'", '+dim+', "'+by+'");'+"\n";
  case 'n':
    return phpVar + ' = ' + '$tk->AddParameterChar("'+io+'", 4, "'+comment+'", "'+varName+'", "'+value+'", "'+varying+'", '+dim+', "'+by+'");'+"\n";
  case '*':
    return '// ' + phpVar + ' = ' + '$tk->Pointer-UnSupported("'+io+'", 10, "'+comment+'", "'+varName+'", "'+value+'", "'+varying+'", '+dim+', "'+by+'");'+"\n";
  case 'h':
    return phpVar + ' = ' + '$tk->AddParameterHole('+rLen+', '+comment+');'+"\n";
  // *** unsupported type
  default:
    return '// ' + phpVar + ' = ' + '$tk->'+ rLen + rType +'-UnSupported("'+io+'", 10, "'+comment+'", "'+varName+'", "'+value+'", "'+varying+'", '+dim+', "'+by+'");'+"\n";
  }
}

/**
 * v1.4.0 added $comment as arg 5, in place of the obsolete $isReturnParam argument. 
 * Data structure return values didn't work properly before 1.4.0 anyway.
 * 
 * @param $paramsArray
 * @param string $struct_name
 * @param int $dim
 * @param string $comment
 * @param string $by
 * @param bool $isArray
 * @param null $labelLen
 * @param string $io
 * __construct($paramsArray, $struct_name ="DataStruct", $dim=0, $comment = '', 
 *             $by='', $isArray=false, $labelLen = null, $io = 'both')
 */
function php_toolkit_DataStructure(phpVar, paramsArray, struct_name, dim, comment, by, isArray, labelLen, io)
{
  var php = '';
  php += phpVar + ' = new DataStructure(';
  php += '$'+paramsArray;
  if (struct_name) 
    php += ', "'+struct_name+'"';
  else
    php += ', "DataStruct"';
  if (dim) 
    php += ', '+dim;
  else
    php += ', 0';
  if (comment) 
    php += ', "'+comment+'"';
  else
    php += ', ""';
  if (by) 
    php += ', "'+by+'"';
  else
    php += ', "ref"'
  if (labelLen)
    php += ', "'+labelLen+'"';
  else
    php += ', null';
  if (io) 
    php += ', "'+io+'"';
  else
    php += ', "both"'
  php += ');';
  return php_newline(php);
}
// AddDataStruct(array $parameters, $name='struct_name', $dim=0, $by='', $isArray=false, $labelLen = null, $comment = '', $io = 'both')
function php_toolkit_DataStructure2(phpVar, paramsArray, struct_name, dim, comment, by) {
  if (PHPFORMAT == "0") {
    return php_toolkit_DataStructure(phpVar, paramsArray, struct_name, dim, comment, by, false, null, null);
  }
  if (!dim) {
    dim="0";
  }
  if (!by) {
    by="";
  }
  return phpVar + ' = ' + '$tk->AddDataStruct($'+paramsArray+', "'+struct_name+'", '+dim+', "'+by+'");'+"\n";
}

/**
 * pgmCall
 *
 * @param string $pgmName Name of program to call, without library
 * @param string $lib Library of program. Leave blank to use library list or current library
 * @param null $inputParam An array of ProgramParameter objects OR XML representing params, to be sent as-is.
 * @param null $returnParam ReturnValue Array of one parameter that's the return value parameter
 * @param null $options Array of other options. The most popular is 'func' indicating the name of a subprocedure or function.
 * @return array|bool
   public function pgmCall($pgmName, $lib, $inputParam = NULL, $returnParam = NULL, $options = NULL)
 */
function php_toolkit_pgmCall(phpVar, pgmName, lib, inputParam, returnParam, options)
{
  var php = '';
  php += phpVar + '->pgmCall(';
  if (pgmName) 
    php += '"'+pgmName+'"';
  else
    php += '"*PGM"';
  if (lib) 
    php += ', "'+lib+'"';
  else
    php += ', null';
  if (inputParam) 
    php += ', '+inputParam+'';
  else
    php += ', null';
  if (returnParam) 
    php += ', '+returnParam+'';
  else
    php += ', null';
  if (options) 
    php += ', '+options+'';
  else
    php += ', null';
  php += ');';
  return php_newline(php);
}
function php_toolkit_pgmCall2(phpVar, pgmName, lib, inputParam, returnParam)
{
  return php_toolkit_pgmCall(phpVar, pgmName, lib, inputParam, returnParam, null);
}
function php_toolkit_srvpgmCall2(phpVar, pgmName, lib, inputParam, returnParam, func)
{
  return php_toolkit_pgmCall(phpVar, pgmName, lib, inputParam, returnParam, 'array("func"=>"'+func+'")');
}
// =========================
// RPG Free
// ==========================
function rpg_free_len(line) {
  var a = '';
  var s = line.split('(');
  if (s.length < 2) {
    return a;
  }
  var t = s[1].split(')');
  var c = t[0].split(':');
  if (c.length > 1) {
    a = c[0].trim();
  } else {
    a = t[0].trim();
  }
  if (isNaN(a)) {
    a = ile_const_value(a);
  }
  return a;
}
function rpg_free_scale(line) {
  var a = '0';
  var s = line.split('(');
  if (s.length < 2) {
    return a;
  }
  var t = s[1].split(')');
  var c = t[0].split(':');
  if (c.length > 1) {
    a = c[1].trim();
  }
  return a;
}
// dcl-c S_IRWXO 7;
// dcl-c SC_OPEN_MAX const(4)
function rpg_free_c(line) {
  var php = '';
  var s = line.split(' ');
  var n = s[1];
  var v = '';
  var p = line.indexOf('(');
  if (p > -1) {
    v = rpg_free_len(line);
  } else {
    v = s[2];
  }
  ile_const_push(n,v);
  // output
  php = '$' + n + ' = ' + v + ';';
  return php_newline(php);
}
// dcl-s runState char(10) inz(*BLANKS);
function rpg_free_s(phpVar,line) {
  var php = '';
  var t = null;
  var id = null;
  var val = null;
  var dim = null;
  var vary = null;
  var likeds = null;
  var ischar = null;
  var io = 'both';
  var by = null;
  var comment = null;
  var inz = '';
  var rLen = '1';
  var rTyp = 'a';
  var rPre = "";
  var s = line.split(' ');
  for (var i = 0; i < s.length; i++) {
    var p = 0;
    var w = s[i].trim();
    var hack = ':' + w + ':';
    // nothing
    if (w.length < 1) {
      continue;
    }
    // dcl-s
    p = w.indexOf('dcl-s');
    if (p > -1) {
      continue;
    }
    // inz(
    p = w.indexOf('inz(');
    if (p > -1) {
      continue;
    }
    // dim(99)
    p = w.indexOf('dim(');
    if (p > -1) {
      dim = rpg_free_len(w);
      continue;
    }
    // likeds(n)
    p = w.indexOf('likeds(');
    if (p > -1) {
      var l = rpg_free_len(w);
      likeds = l;
      inz = '';
      continue;
    }
    // like(n)
    p = w.indexOf('like(');
    if (p > -1) {
      var l = rpg_free_len(w);
      likeds = l;
      inz = '';
      continue;
    }
    // varchar(n)
    p = w.indexOf('varchar(');
    if (p > -1) {
      var l = rpg_free_len(w);
      var s = rpg_free_scale(w);
      t = l + 'a';
      if (s == '0') {
        vary = '2';
      } else {
        vary = s;
      }
      ischar = t;
      inz = '';
      // type
      rLen = l;
      rTyp = 'a';
      rPre = "";
      continue;
    }
    // char(n)
    p = w.indexOf('char(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = l + 'a';
      ischar = t;
      inz = '';
      // type
      rLen = l;
      rTyp = 'a';
      rPre = "";
      continue;
    }
    // float(n)
    p = w.indexOf('float(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = l + 'f';
      inz = 0.0;
      // type
      rLen = l;
      rTyp = 'f';
      rPre = "";
      continue;
    }
    // int(n)
    p = w.indexOf('int(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = l + 'i0';
      inz = 0;
      // type
      rLen = l;
      rTyp = 'i';
      rPre = 0;
      continue;
    }
    // uns(n)
    p = w.indexOf('uns(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = l + 'u0';
      inz = 0;
      // type
      rLen = l;
      rTyp = 'u';
      rPre = 0;
      continue;
    }
    // packed(n:n)
    p = w.indexOf('packed(');
    if (p > -1) {
      var l = rpg_free_len(w);
      var s = rpg_free_scale(w);
      t = l + 'p' + s;
      inz = 0.0;
      // type
      rLen = l;
      rTyp = 'p';
      rPre = s;
      continue;
    }
    // zoned(n:n)
    p = w.indexOf('zoned(');
    if (p > -1) {
      var l = rpg_free_len(w);
      var s = rpg_free_scale(w);
      t = l + 's' + s;
      inz = 0.0;
      // type
      rLen = l;
      rTyp = 's';
      rPre = s;
      continue;
    }
    // bindec(n:n)
    p = w.indexOf('bindec(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = l + 'b';
      inz = 0;
      // type
      rLen = l;
      rTyp = 'b';
      rPre = "";
      continue;
    }
    // date()
    p = w.indexOf('date(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = '10a';
      inz = 0;
      // type
      rLen = 10;
      rTyp = 'a';
      rPre = "";
      continue;
    }
    // time()
    p = w.indexOf('time(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = '8a';
      inz = 0;
      // type
      rLen = 8;
      rTyp = 'a';
      rPre = "";
      continue;
    }
    // timestamp()
    p = w.indexOf('timestamp(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = '26a';
      inz = 0;
      // type
      rLen = 26;
      rTyp = 'a';
      rPre = "";
      continue;
    }
    // hole(n)
    p = w.indexOf('hole(');
    if (p > -1) {
      var l = rpg_free_len(w);
      t = l + 'h';
      inz = 0;
      // type
      rLen = l;
      rTyp = 'h';
      rPre = "";
      continue;
    }
    // pointer()
    p = hack.indexOf(':pointer:');
    if (p > -1) {
      t = '64h';
      inz = 0;
      // type
      rLen = "";
      rTyp = 'unsupported-pointer';
      rPre = "";
      continue;
    }
    // ind
    p = hack.indexOf(':ind:');
    if (p > -1) {
      t = '4a';
      inz = "";
      // type
      rLen = 4;
      rTyp = 'a';
      rPre = "";
      continue;
    }
    // unsupported
    p = w.indexOf('unsupported(');
    if (p > -1) {
      // type
      rLen = "";
      rTyp = w;
      rPre = "";
      continue;
    }
    // unsupported
    p = w.indexOf('(');
    if (!likeds && p > -1) {
      var l = rpg_free_len(w);
      t = 'unsupported';
      inz = "";
      // type
      rLen = "";
      rTyp = "unsupported-" + w;
      rPre = "";
      continue;
    }
    // dcl-s name
    if (!id) {
      id = w;
    }
  } // end for
  // output
  if (likeds) {
    php += php_toolkit_DataStructure2(phpVar, likeds, id, dim, comment, by);
  } else {
    php += php_toolkit_ProgramParameter2(phpVar, rLen, rTyp, rPre, io, id, id, inz, vary, dim, by);
  }
  return php;
}
// dcl-ds iobj_t qualified based(Template);
function rpg_free_ds(phpVar,line,flag) {
  var php = '';
  var id = null;
  var dim = null;
  var io = 'both';
  var by = null;
  var comment = null;
  var s = line.split(' ');
  for (var i = 0; i < s.length; i++) {
    var p = 0;
    var w = s[i].trim();
    // nothing
    if (w.length < 1) {
      continue;
    }
    // dcl-ds
    p = w.indexOf('dcl-ds');
    if (p > -1) {
      continue;
    }
    // dim(99)
    p = w.indexOf('dim(');
    if (p > -1) {
      dim = rpg_free_len(w);
      continue;
    }
    // dcl-s name
    if (!id) {
      id = w;
      if (flag) {
        var newVar = '$' + id + "[]";
        return newVar;
      }
    }
  } // end for
  // output
  php += php_toolkit_DataStructure2(phpVar, id, id, dim, comment, by);
  return php;
}
// dcl-pr xjOutFmt varchar(20);
function rpg_free_pr(line) {
  var php = '';
  var id = null;
  var pgm = null;
  var lib = null;
  var inputParam = '$parm';
  var returnParam = null;
  var s = line.split(' ');
  for (var i = 0; i < s.length; i++) {
    var p = 0;
    var w = s[i].trim();
    // nothing
    if (w.length < 1) {
      continue;
    }
    // dcl-pr
    p = w.indexOf('dcl-pr');
    if (p > -1) {
      continue;
    }
    // dcl-pr name
    if (!id) {
      id = w.toUpperCase();
    } else {
      returnParam = '$ret';
      // output
      php += php_var_null(returnParam);
      php += rpg_free_s(returnParam+"[]", 'rc ' + w);
      break;
    }
  } // end for
  if (ILEPGM) {
    pgm = ILEPGM;
  }
  if (ILELIB && ILELIB != "*LIBL") {
    lib = ILELIB;
  }
  // output
  php += php_toolkit_srvpgmCall2('$tk', pgm, lib, inputParam, returnParam, id);
  return php;
}

function rpg_free_2_php_toolkit(rpgfree,dcl_s) {
  var php = '';
  var line = '';
  var big = '';
  var n = 0;
  var inC = false;
  var inS = false;
  var inDs = false;
  var inPr = false;
  var saveMe = '';
  // parse 'interesting' lines terminated with ';'
  var rpg = rpgfree.split('\n');
  for(var i = 0; i < rpg.length; i++) {
    line = rpg[i].toLowerCase().trim();
    // blank line
    if (line.length < 1) {
      continue;
    }
    // comment
    var c = line.charAt(0);
    switch (c) {
    case '*':
      continue;
      break;
    case '/':
      continue;
      break;
    default:
      break;
    }
    // end line comment
    n = line.indexOf('//');
    if (n > -1) {
      var bbb = line.split('//');
      line = bbb[0].trim();
    }
    // dcl-ds
    n = line.indexOf('dcl-ds');
    if (n > -1) {
      inDs = true;
    }
    if (inDs) {
      big += line + ' ';
    }
    n = line.indexOf('end-ds');
    if (n > -1) {
      inDs = false;
    }
    if (inDs) {
      continue;
    }
    // dcl-pr
    n = line.indexOf('dcl-pr');
    if (n > -1) {
      inPr = true;
    }
    if (inPr) {
      big += line + ' ';
    }
    n = line.indexOf('end-pr');
    if (n > -1) {
      inPr = false;
    }
    if (inPr) {
      continue;
    }
    // dcl-s
    n = line.indexOf('dcl-s');
    if (n > -1) {
      inS = true;
    }
    if (inS) {
      big += line + ' ';
    }
    n = line.indexOf(';');
    if (n > -1) {
      inS = false;
    }
    if (inS) {
      continue;
    }
    // dcl-c
    n = line.indexOf('dcl-c');
    if (n > -1) {
      inC = true;
    }
    if (inC) {
      big += line + ' ';
    }
    n = line.indexOf(';');
    if (n > -1) {
      inC = false;
    }
    if (inC) {
      continue;
    }
  }
  // parse lines on ';'
  var rpg = big.split(';');
  php += php_var_null('$parm[]');
  for(var i = 0; i < rpg.length; i++) {
    var phpVar = '$parm[]';
    line = rpg[i].toLowerCase().trim();
    // dcl-ds iobj_t qualified based(Template);
    //   isa int(10);
    //   gattr char(G_FMT_KEY_CHAR) dim(G_JUNK_MAX_ATTR);
    //   gvalue char(G_FMT_KEY_CHAR) dim(G_JUNK_MAX_ATTR);
    // end-ds;
    n = line.indexOf('dcl-ds');
    if (n > -1) {
      // new DataStructure
      php += php_newline("// DataStructure");
      saveMe = rpg_free_ds(phpVar,line,false);
      phpVar = rpg_free_ds(phpVar,line,true);
      php += php_var_null(phpVar);
      //   isa int(10);
      //   gattr char(G_FMT_KEY_CHAR) dim(G_JUNK_MAX_ATTR);
      //   gvalue char(G_FMT_KEY_CHAR) dim(G_JUNK_MAX_ATTR);
      // end-ds;
      for(i++; i < rpg.length; i++) {
        line = rpg[i].toLowerCase().trim() + ' ';
        // end-ds;
        n = line.indexOf('end-ds');
        if (n > -1) {
          php += saveMe;
          break;
        }
        php += rpg_free_s(phpVar,line);
      }
    }
    //   dcl-proc xjValueFactory export;
    //   dcl-pi *N ind;
    //     key char(G_FMT_KEY_CHAR) value;
    //     level int(10) value;
    //     attach likeds(ipc_attach_t);
    //     val1_off int(10) value;
    //     val2_off int(10) value;
    //   end-pi;
    n = line.indexOf('dcl-proc');
    if (n > -1) {
      // new Prototype
      php += php_newline("// Program");
      php += php_var_null(phpVar);
      var rc = line.split(' ');
      saveMe = rc[0] + ' ' + rc[1];
      //   dcl-pi *N ind;
      i++;
      line = rpg[i].toLowerCase().trim();
      rc = line.split(' ');
      if (rc.length > 0) {
        saveMe += ' ' + rc[rc.length - 1];
      }
      //     key char(G_FMT_KEY_CHAR) value;
      //     level int(10) value;
      //     attach likeds(ipc_attach_t);
      //     val1_off int(10) value;
      //     val2_off int(10) value;
      //   end-pi;
      for(i++; i < rpg.length; i++) {
        line = rpg[i].toLowerCase().trim() + ' ';
        // end-pi;
        n = line.indexOf('end-pi');
        if (n > -1) {
          php += rpg_free_pr(saveMe);
          nullParm = true;
          break;
        }
        php += rpg_free_s(phpVar,line);
      }
    }
    // dcl-pr xjOutFmt varchar(20);
    //   this likeds(iobj_t);
    // end-pr;
    n = line.indexOf('dcl-pr');
    if (n > -1) {
      // new Prototype
      php += php_newline("// Program");
      php += php_var_null(phpVar);
      saveMe = line;
      //     this likeds(iobj_t);
      // end-pr;
      for(i++; i < rpg.length; i++) {
        line = rpg[i].toLowerCase().trim() + ' ';
        // end-pr;
        n = line.indexOf('end-pr');
        if (n > -1) {
          php += rpg_free_pr(saveMe);
          nullParm = true;
          break;
        }
        php += rpg_free_s(phpVar,line);
      }
    }
    // dcl-s runState char(10) inz(*BLANKS);
    n = line.indexOf('dcl-s');
    if (n > -1 && dcl_s) {
      // new ProgramParameter
      php += php_newline("// ProgramParameter");
      php += rpg_free_s(phpVar,line);
    }
    // dcl-c SC_OPEN_MAX const(4);
    n = line.indexOf('dcl-c');
    if (n > -1) {
      // new Constant
      if (dcl_s) {
        php += php_newline("// Constant");
        php += rpg_free_c(line);
      } else {
        rpg_free_c(line);
      }
    }
  }
  return php;
}
function rpg_free_sample() {
  var rpg = ''; 
  rpg +="       dcl-c G_FMT_KEY_CHAR const(\n";
  rpg +="       16\n";
  rpg +="       );\n";
  rpg +="       dcl-c G_FMT_CHAR 5;\n";
  rpg +="       dcl-c G_JUNK_MAX_ATTR const(8);\n";
  rpg +="       dcl-ds iobj_t qualified based(Template);\n";
  rpg +="         isa int(10);\n";
  rpg +="         gattr char(G_FMT_KEY_CHAR) dim(G_JUNK_MAX_ATTR);\n";
  rpg +="         gvalue char(G_FMT_KEY_CHAR) dim(G_JUNK_MAX_ATTR);\n";
  rpg +="       end-ds;\n";
  rpg +="       dcl-ds i1_t qualified based(Template);\n";
  rpg +="         isa int(10);\n";
  rpg +="         iobj likeds(iobj_t) dim(99);\n";
  rpg +="       end-ds;\n";
  rpg +="       dcl-ds i2_t qualified based(Template);\n";
  rpg +="         isa int(10);\n";
  rpg +="         i1 likeds(i1_t) dim(99);\n";
  rpg +="       end-ds;\n";
  rpg +="       dcl-s runState char(10) inz(*BLANKS);\n";
  rpg +="       dcl-s nextState char(10) inz(*BLANKS);\n";
  rpg +="       dcl-s errState char(10) inz(*BLANKS);\n";
  rpg +="       dcl-ds qtqCode_t qualified based(Template);\n";
  rpg +="         qtqCCSID int(10);\n";
  rpg +="         qtqAltCnv int(10);\n";
  rpg +="         qtqAltSub int(10);\n";
  rpg +="         qtqAltSft int(10);\n";
  rpg +="         qtqOptLen int(10);\n";
  rpg +="         qtqMixErr int(10);\n";
  rpg +="         qtqRsv int(20);\n";
  rpg +="       end-ds;\n";
  rpg +="       dcl-ds ciconv_t qualified based(Template);\n";
  rpg +="         conviok int(10);\n";
  rpg +="         conv likeds(iconv_t);\n";
  rpg +="         tocode likeds(qtqCode_t);\n";
  rpg +="         fromcode likeds(qtqCode_t);\n";
  rpg +="       end-ds;\n";
  rpg +="       dcl-pr xjOutFmt varchar(20);\n";
  rpg +="         this likeds(iobj_t);\n";
  rpg +="       end-pr;\n";
  rpg +="       dcl-ds ipc_attach_t qualified based(Template);\n";
  rpg +="         gthis pointer;\n";
  rpg +="         iAmServer ind;\n";
  rpg +="         fmtIn varchar(G_FMT_CHAR);\n";
  rpg +="         fmtOut varchar(G_FMT_CHAR);\n";
  rpg +="         ctlKey pointer;\n";
  rpg +="         ctlKeyLen int(10);\n";
  rpg +="         ctlIpc pointer;\n";
  rpg +="         ctlIpcLen int(10);\n";
  rpg +="         dataIn pointer;\n";
  rpg +="         dataInLen int(10);\n";
  rpg +="         dataOut pointer;\n";
  rpg +="         dataOutLen int(10);\n";
  rpg +="         ipcSemKey int(10);\n";
  rpg +="         ipcSemRet int(10);\n";
  rpg +="       end-ds;\n";
  rpg +="       dcl-proc xjValueFactory export;\n";
  rpg +="       dcl-pi *N ind;\n";
  rpg +="         key char(G_FMT_KEY_CHAR) value;\n";
  rpg +="         level int(10) value;\n";
  rpg +="         attach likeds(ipc_attach_t);\n";
  rpg +="         val1_off int(10) value;\n";
  rpg +="         val2_off int(10) value;\n";
  rpg +="       end-pi;\n";
  return rpg;
}

// =========================
// RPG D Spec
// =========================
function rpg_d_s(rTyp, rLen, rPre, rVary)
{
  var p = 0;
  var rpgfree = '';
  if (rTyp.trim().length < 1) {
    return rpgfree;
  }
  if (!rLen.length) rLen = '0';
  if (!rPre.length) rPre = '0';
  switch (rTyp) {
  case "i":
    rpgfree = "int(" + rLen + ")";
    break;
  case "u":
    rpgfree = "uns(" + rLen + ")";
    break;
  case "f":
    rpgfree = "float(" + rLen + ")";
    break;
  case "p":
    rpgfree = "packed(" + rLen + ":" + rPre + ")";
    break;
  case "s":
    rpgfree = "zoned(" + rLen + ":" + rPre + ")";
    break;
  case "b":
    rpgfree = "bindec(" + rLen + ")";
    break;
  case "a":
    if (rVary) {
      rpgfree = "varchar(" + rLen + ":" + rVary + ")";
    } else {
      rpgfree = "char(" + rLen + ")";
    }
    break;
  case "d":
    rpgfree = "char(10)";
    break;
  case "t":
    rpgfree = "char(8)";
    break;
  case "z":
    rpgfree = "char(26)";
    break;
  case "n":
    rpgfree = "ind";
    break;
  case "*":
    rpgfree = "pointer";
    break;
  case "h":
    rpgfree = "hole("+ rLen + ")";
    break;
  // *** unsupported type
  default:
    rpgfree = "unsupported(" + rLen + rTyp +")";
    break;
  }
  return rpgfree;
}

function rpg_d_2_php_toolkit(rpgdspec, rFlag) 
{
  var rpgfree = "";
  var longName = "";
  var isPI = false;
  var isGo = false;
  var isFirst = false;
  // ----------------
  // split d spec on lf
  rpgdspec += "     D deadbeef        s             10A   inz(*BLANKS)\n";
  var rpg = rpgdspec.split('\n');
  for(var i = 0; i < rpg.length; i++) {
    var line = rpg[i];
    if (!line.trim().length) continue;
    // please do not trim, but they ignore and trim anyway
    if (line.charAt(0).toUpperCase()  == 'D') {
      line = '     ' + line;
    }
    // D*i am comment
    var rDComment = line.substr(6, 1).toUpperCase();
    if (rDComment == '*') {
      continue;
    }
    var rDSpec = line.substr(5, 1).toUpperCase();
    if (rDSpec != 'D') {
      continue;
    }
    var dotPos = line.indexOf("...");
    if (dotPos > 0) {
      var partName = line.substr(6,dotPos-6);
      longName += partName.trim();
      continue;
    }
    var rName = line.substr(6, 15).trim(); // was 6, 17 - Sam remove 'e ds'
    if (longName.length) {
      rName = longName + rName;
      longName = '';
    }
    if (!rName.length) {
      rName = 'missing';
    }
    // remove start name '$', '@', '#'
    var rDollar = rName.substr(0, 1);
    if (rDollar == "$" || rDollar == "@" || rDollar == "#") {
      rName = rName.substr(1, rName.length-1);
    }
    var rName_t = rName;
    var rDS   = line.substr(23, 2).toUpperCase().trim();
    // start of DS,S,PR,PI, etc.?
    if (rDS) {
      for(var j = 0; j < 2; j++) {
        // working collect?
        if (!isGo || j) {
          isGo = rDS;
          isFirst = true;
          // false end
          if (rName == "deadbeef") {
            break;
          }
          switch (isGo) {
          case "PR":
            rpgfree += "dcl-pr " + rName;
            break;
          case "PI":
            rpgfree += "dcl-proc " + rName;
            isPI = true;
            break;
          case "DS":
            rpgfree += "dcl-ds " + rName;
            break;
          case "C":
            rpgfree += "dcl-c " + rName;
            break;
          case "S":
            rpgfree += "dcl-s " + rName;
            break;
          default:
            break;
          }
          j = 2;
        // collect complete
        } else {
          switch (isGo) {
          case "PR":
            rpgfree += "end-pr;\n";
            break;
          case "PI":
            rpgfree += "end-pi;\n";
            isPI = true;
            break;
          case "DS":
            rpgfree += "end-ds;\n";
            break;
          case "C":
            break;
          case "S":
           break;
          default:
            break;
          }
          isGo = false;
          // false end
          if (rName == "deadbeef") {
            break;
          }
        }
      }
      // false end
      if (rName == "deadbeef") {
        break;
      }
    }
    var rLen  = line.substr(25, 14).trim();
    var rTyp  = line.substr(39, 1).toLowerCase();
    var rPre  = line.substr(40, 3).trim();
    var rOpt  = line.substr(43, 40).trim();
    // collect options likeds(myds) dim(999) varying varying(4) ...
    for(var j = i + 1; j < rpg.length; j++) {
      var j_line = rpg[j];
      if (!j_line.trim().length) break;
      // please do not trim, but they ignore and trim anyway
      if (j_line.charAt(0).toUpperCase()  == 'D') {
        j_line = '     ' + j_line;
      }
      j_line += "                                        ";
      // D*i am comment
      var j_DComment = j_line.substr(6, 1).toUpperCase();
      if (j_DComment == '*') {
        continue;
      }
      var j_DSpec = j_line.substr(5, 1).toUpperCase();
      if (j_DSpec != 'D') {
        break;
      }
      var j_opt = j_line.substr(6,37).trim();
      if (j_opt.length) {
        break;
      }
      rOpt += ' ' + j_line.substr(43, 40).trim();
      i = j;
    }
    // force all options to lower case
    var rOptl = rOpt.toLowerCase();
    // export mixed case ExtProc('ProgramA')
    // export missing is UPPER
    var rExtProc = rName.toUpperCase();
    var rExtProc0 = rOptl.indexOf('extproc(');
    if (rExtProc0 > -1) {
      var rExtProc1 = rOpt.indexOf("'",rExtProc0);
      var rExtProc2 = rOpt.indexOf("')",rExtProc0);
      if (rExtProc1 > -1 && rExtProc2 > -1 && rExtProc2 > rExtProc1 + 1) {
        rExtProc1 += 1;
        rExtProc = rOpt.substr(rExtProc1, rExtProc2 - rExtProc1).trim();
      }
    }
    // like(myds) or likeds(myds)
    var rLike = '';
    var rLike0 = rOptl.indexOf('like(');
    if (rLike0 < 0) rLike0 = rOptl.indexOf('likeds(');
    if (rLike0 > -1) {
      var rLike1 = rOpt.indexOf('(',rLike0);
      var rLike2 = rOpt.indexOf(')',rLike0);
      if (rLike1 > -1 && rLike2 > -1 && rLike2 > rLike1 + 1) {
        rLike1 += 1;
        rLike = rOpt.substr(rLike1, rLike2 - rLike1).trim();
      }
    }
    var rLike_t = rLike;
    // dim(999), occurs(200)
    var rDim  = 1;
    var rDim0 = rOptl.indexOf('dim(');
    if (rDim0 < 0) rDim0 = rOptl.indexOf('occurs(');
    if (rDim0 > -1) {
      var rDim1 = rOptl.indexOf('(',rDim0);
      var rDim2 = rOptl.indexOf(')',rDim0);
      if (rDim1 > -1 && rDim2 > -1 && rDim2 > rDim1 + 1) {
        rDim1 += 1;
        rDim = parseInt(rOptl.substr(rDim1, rDim2 - rDim1).trim());
      }
    }
    // varying, varying(2), varying(4)
    var rVary  = 0;
    var rVary0 = rOptl.indexOf('varying');
    if (rVary0 > -1) {
      rVary0 = rOptl.indexOf('varying(');
      if (rVary0 > -1) {
        var rVary1 = rOptl.indexOf('(',rVary0);
        var rVary2 = rOptl.indexOf(')',rVary0);
        if (rVary1 > -1 && rVary2 > -1 && rVary2 > rVary1 + 1) {
          rVary1 += 1;
          rVary = parseInt(rOptl.substr(rVary1, rVary2 - rVary1).trim());
        }
      } else {
        rVary  = 2;
      }
    }
    // no type (crazy rpg rules)
    // D zzold1          PI
    // D  Len                           2  0 ... packed(2p0)
    // D  Frog                         10    ... char(10)
    // D $vevsods        ds                  occurs(200) ... dim(200)
    // D $vsukz                  1      1    ... char(1a) 
    // D $vpos                   2      9    ... char(8a)
    // D $vtxt                  10     39    ... char(30a)
    // D $vkalw                 40    174  2 dim(15) ... zoned(9s2) dim(15)
    if (!rTyp.trim().length && (!rDS.length || rDS == 'S')) {
      if (rPre.length) {
        if (rDS.length || isPI) { // no type in S or PI/PR is packed 
          rTyp = 'p';
        } else { // no type in DS is zoned
          rTyp = 's';
        }
      } else { // no type and no precision
        if (rLike.length) { // likeds(mytype_t)
          rTyp = '';
        }
        else { // no type and no precision is character
          rTyp = 'a';
        }
      }
    }
    // RPG III no type (more crazy rpg rules)
    // D $vkalw                 40    174  2 dim(15) ... 9s2 dim(15)
    if (rLen.indexOf(' ') > 0) {
      var rLen1 = parseInt(line.substr(25, 7).trim());
      var rLen2 = parseInt(line.substr(32, 7).trim());
      if (rLen2 >= rLen1) {
        var rLen3 = (rLen2 - rLen1 + 1)/rDim;
        rLen = rLen3.toString();
      }
    }
    // collect
    var adjOpt = rOpt.trim();
    if (adjOpt.length) {
      var tmp = adjOpt.split(' ');
      adjOpt = '';
      for (j=0; j < tmp.length; j++) {
        var tl = tmp[j].trim();
        if (tl.indexOf('vary') > -1) {
          continue;
        }
        adjOpt += tl + ' ';
      }
      if (adjOpt.length) {
        adjOpt = ' ' + adjOpt.trim();
      }
    }
    // ----------------
    // convert to rpg free
    switch (isGo) {
    case "PR":
      if (isFirst) {
        if (rTyp) {
          rpgfree += ' ' + rpg_d_s(rTyp, rLen, rPre, rVary);
        }
        rpgfree += adjOpt + ";\n";
      } else {
        rpgfree += '  ' + rName + ' ' + rpg_d_s(rTyp, rLen, rPre, rVary) + adjOpt + ";\n";
      }
      break;
    case "PI":
      if (isFirst) {
        rpgfree += adjOpt + ";\n";
        rpgfree += 'dcl-pi *N';
        if (rTyp) {
          rpgfree += ' ' + rpg_d_s(rTyp, rLen, rPre, rVary);
        }
        rpgfree += adjOpt + ";\n";
      } else {
        rpgfree += '  ' + rName + ' ' + rpg_d_s(rTyp, rLen, rPre, rVary) + adjOpt + ";\n";
      }
      break;
    case "DS":
      if (isFirst) {
        rpgfree += adjOpt + ";\n";
      } else {
        rpgfree += '  ' + rName + ' ' + rpg_d_s(rTyp, rLen, rPre, rVary) + adjOpt + ";\n";
      }
      break;
    case "C":
      if (isFirst) {
        rpgfree += ' ' + rpg_d_s(rTyp, rLen, rPre, rVary) + adjOpt + ";\n";
      }
      break;
    case "S":
      if (isFirst) {
        rpgfree += ' ' + rpg_d_s(rTyp, rLen, rPre, rVary) + adjOpt + ";\n";
      }
      break;
    default:
      break;
    }
    isFirst = false;
  }
  // output
  switch (rFlag) {
  case 1:
    return rpg_free_2_php_toolkit(rpgfree,false);
  case 2:
    return rpg_free_2_php_toolkit(rpgfree,true);
  default:
    break;
  }
  return rpgfree;
}
function rpg_dspec_sample() {
  var rpg = '';
  rpg +="     D dcRec           ds                  qualified based(Template)\n";
  rpg +="     D  dcMyName                     10A\n";
  rpg +="     D somejunk        s             10A   inz(*BLANKS)\n";
  rpg +="     Dzzbigi           PR            20u 0\n";
  rpg +="     Dmmint8                          3i 0\n";
  rpg +="     Dmrec                                 likeds(dcRec)\n";
  rpg +="     DdoRec_t          ds                  qualified based(Template)\n";
  rpg +="     D doFrog                        10i 0\n";
  rpg +="     D doToad                         1N\n";
  rpg +="\n";
  rpg +="     D justJunk        s            128A\n";
  rpg +="\n";
  rpg +="     DipcRec_t         ds                  qualified based(Template)\n";
  rpg +="     D ipcOwnKey                    128A\n";
  rpg +="     D ipcFlags                            like(doRec_t)\n";
  rpg +="\n";
  rpg +="     D cpyIn           PR             1N\n";
  rpg +="     D   node                              likeds(ipcRec_t)\n";
  rpg +="     D                                     dim(999)\n";
  rpg +="\n";
  rpg +="     D sql_active_any...        \n";
  rpg +="     D _large_...        \n";
  rpg +="     D frog            PI             1N\n";
  rpg +="     D  type                          1A\n";
  rpg +="     D  label                        10A\n";
  rpg +="     D                                     dim(10)\n";
  rpg +="     D                                     varying(4)\n";
  rpg +="     D  type1                         1A\n";
  rpg +="\n";
  rpg +="     D zzold1          PI\n";
  rpg +="     D  Len                           2  0\n";
  rpg +="     D  Wid                           2  0\n";
  rpg +="     D  Area                          4  0\n";
  rpg +="     D  Frog                         10\n";
  rpg +="     D $vevsfi         s              1\n";
  rpg +="     D $vevsrj         s              2\n";
  rpg +="     D $vevsob         s              7s 0\n";
  rpg +="     D $vevsve         s              5s 0\n";
  rpg +="     D*Ergebnisdaten:\n";
  rpg +="     D $vevsods        ds                  occurs(200)\n";
  rpg +="     D $vsukz                  1      1\n";
  rpg +="     D $vpos                   2      9\n";
  rpg +="     D $vtxt                  10     39\n";
  rpg +="     D $vkalw                 40    174  2 dim(15)\n";
  rpg +="     D $vvsw                 175    309  2 dim(15)\n";
  rpg +="     D $vvsk                 310    324  0 dim(15)\n";
  rpg +="     d*\n";
  rpg +="     D i               S             10i 0 inz(0)\n";
  rpg +="     D j               S             10i 0 inz(0)\n";
  rpg +="\n";
  rpg +="     D zzvary          PI            20A   varying\n";
  rpg +="     D  myName                       10A   varying\n";
  rpg +="\n";
  rpg +="     D zzvary4         PR            20A   varying(4)\n";
  rpg +="     D  myName                       10A   varying(4)\n";
  rpg +="\n";
  rpg +="     Dmebun            ds                  qualified based(Template)\n";
  rpg +="     D ipcOwnBun                    128A\n";
  rpg +="     D ipcOwnOven                   128A\n";
  rpg +="\n";
  rpg +="     Dmebuns           ds                  likeds(dcRec) dim(23)\n";
  rpg +="\n";
  rpg +="     D zzbun           PR                  likeds(mebun)\n";
  rpg +="     D  myName                       10A   varying(4)\n";
  rpg +="     Dmebunso                              likeds(dcRec) dim(23)\n";
  rpg +="\n";
  rpg +="     D zznoret         PR\n";
  rpg +="     D  myName                       10A   varying(4)\n";
  rpg +="\n";
  rpg +="     D zznoparam       PR            10a   varying\n";
  rpg +="\n";
  rpg +="     D zznothing       PR\n";
  rpg +="\n";
  rpg +="     D pref_build      PR             5P 0 ExtProc('ProgramA')\n";
  rpg +="     D  webpgmtype                    1A\n";
  rpg +="     D XML_MSG_SQL_FAIL...\n";
  rpg +="     D                 c                   const('SQL fail')\n";
  rpg +="     D XML_MSG_SQL_EXCEPTION...\n";
  rpg +="     D                 c                   const('SQL excp')\n";
  rpg +="     P runASCII        B                   export\n";
  rpg +="     D runASCII        PI             1N\n";
  rpg +="     D   pIPCSP2                       *\n";
  rpg +="     D   szIPCSP2                    10i 0\n";
  rpg +="     D   pCtlSP2                       *\n";
  rpg +="     D   szCtlSP2                    10i 0\n";
  rpg +="     D   pIClob2                       *\n";
  rpg +="     D   szIClob2                    10i 0\n";
  rpg +="     D   pOClob2                       *\n";
  rpg +="     D   szOClob2                    10i 0\n";
  rpg +="     D   ccsidPASE2                  10i 0\n";
  rpg +="     D   ccsidILE2                   10i 0\n";
  rpg +="      * vars\n";
  rpg +="     D szIn            s             10i 0 inz(0)\n";
  rpg +="     D szOut           s             10i 0 inz(0)\n";
  rpg +="     D runGood         s              1N   inz(*OFF)\n";
  rpg +="     D pIPC            s           1024A   inz(*BLANKS)\n";
  rpg +="     D pCtl            s           1024A   inz(*BLANKS)\n";
  return rpg;
}


// =========================
// Cobol - period at the end that's the COBOL separator
// Columns 1-6 in most COBOL layouts are ignored by the compiler, 
// as is everything after column 72
// Continuation lines (An “-” in column 7)
// Comment lines (An “*” or “/” in column 7)
// Debugging lines (An “D” in column 7)
// Area A (Columns 8 – 11 )
//    You can usually find your place in the layout from the 01 level, 
//    which normally starts in column 8.  
//    All other levels should start in column 12 or above
//    Division headers
//    Section headers
//    Paragraph headers or paragraph names
//    Level indicators or level-numbers (01 and 77)
//    DECLARATIVES and END DECLARATIVES
//    End program, end class, and end method markers
// Area B (Columns 12 through 72)
//    Entries, sentences, statements, and clauses
//    Continuation lines
// Documents:
// https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_61/rzase/sc092540417.htm
// https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_72/rzase/cblcx2styp.htm
// http://www.3480-3590-data-conversion.com/article-reading-cobol-layouts-1.html
// =========================
function cobol_2_php_toolkit(cobol, rFlag) 
{
  var cobolfree = "";
  var inLine = false;
  var c = "";
  // ----------------
  // collect single line cobol (end '.')
  var cbl = cobol.split('\n');
  for(var i = 0; i < cbl.length; i++) {
    var line = cbl[i];
    var tline = line.trim();
    if (!tline.length) continue;
    // no trim, trim anyway (argh)
    c = line.charAt(0);
    if (c != ' ') {
      line = '       ' + line;
    }
    // remove comment
    if (line.length > 71) {
      line = line.substr(0,71);
    }
    // remove label
    var label = line.substr(0,6);
    if (label.trim().length) {
      line = '       ' + line.substr(7);
    }
    // collecting line?
    if (inLine) {
      cobolfree += ' ' + line;
    } else {
      // 01 - 49, 77 ?
      c = tline.charAt(0);
      if (!isNaN(c)) {
        cobolfree += line;
        inLine = true;
      } else {
        var p = tline.indexOf('COPY DD');
        if (p > -1) {
          cobolfree += line;
          inLine = true;
        }
      }
    }
    // end of line?
    tline = cobolfree.trim();
    if (!tline.length) continue;
    c = tline.charAt(tline.length-1);
    if (inLine && c == '.') {
      inLine = false;
      cobolfree += "\n";
    } 
  }
  // ----------------
  // convert to rpg free
  var rpgfree = "";
  var cbl = cobolfree.split('\n');
  var isDS = false;
  var isData = false;
  var dataName = "";
  var dsName = "";
  var i = 0;
  var j = 0;
  var k = 0;
  var p = 0;
  var q = 0;
  for(i = 0; i < cbl.length; i++) {
    var line = cbl[i].toUpperCase();
    var tline = line.trim();
    // split statement on ' '
    var s = tline.split(' ');
    //  01  RPT1.
    var ds = s[0];
    switch (ds) {
    case '01':
      if (isDS) {
        rpgfree += "end-ds;\n";
      }
      ile_var_clear();
      isDS = true;
      dsName = 'MISSING';
      if (s.length > 1) {
        for(j = 1; j < s.length; j++) {
          var tds = s[j].trim();
          if (tds < 1) continue;
          dsName = ile_unique_name(tds);
          break;
        }
      }
      rpgfree += "dcl-ds " + dsName + ";\n";
      break;
    case '77':
      if (isDS) {
        rpgfree += "end-ds;\n";
      }
      isDS = false;
      break;
    default:
      break;
    }
    // COPY DDS-PROJECT OF CORPDATA-PROJECT.
    p = tline.indexOf('COPY DD');
    if (p > -1) {
      if (isDS) {
        rpgfree += "  ";
      }
      rpgfree += "// unsupported-" + tline + "\n";
    }
    // 05  SALARY    PIC S9(6)V99 PACKED-DECIMAL.
    p = tline.indexOf('PIC');
    if (p > -1) {
      var prefix = '  ';
      if (!isDS) {
        prefix = 'dcl-s ';
      }
      // rpg free (debug)
      if (rFlag == 99) {
        if (isDS) {
          rpgfree += "  ";
        }
        rpgfree += "// " + tline.substr(0,71) + "\n";
      }
      isData = true;
      var lastName = "";
      var len = 0;
      var scale = 0;
      dataName = 'MISSING';
      var isDone = false;
      for(j = 0; !isDone && j < s.length; j++) {
        var ts = s[j].trim();
        if (ts.length < 1) continue;
        // 05  SALARY    PIC
        if (s[j] == 'PIC') {
          dataName = ile_unique_name(lastName);
          // 05  SALARY    PIC 'type'
          for(j++; j < s.length; j++) {
            ts = s[j].trim();
            if (ts.length < 1) continue;
            break;
          }
          c = ts.charAt(0);
          // type convert
          switch(c) {
          //  05  NAME      PIC X(30).
          //  05  FILLER PIC XXX VALUE SPACES.
          case 'X':
            // PIC X(30)
            p = ts.indexOf('(');
            if (p > -1) {
              len = rpg_free_len(ts);
            // PIC XXX
            } else {
              len = 0;
              for(k=0; k < ts.length; k++) {
                c = ts.charAt(k);
                if (c == 'X') {
                  len += 1;
                } else {
                  break;
                }
              } // k loop
              len = len.toString();
            }
            // rpg free
            rpgfree += prefix + dataName + ' ' + rpg_d_s('a', len, "", null) + ";\n";
            isDone = true;
            break;
          // Note: Not sure these types correct (need cobol help)
          //  05  EMPLOYEE-COUNT PIC ZZZ9.
          //  05  TOTAL-PROJ-COST PIC ZZZZZZZZ9.99.
          case 'Z':
            var zdone = false;
            var zdot = 0;
            var zinz = "";
            len = 0;
            for (k=0; !zdone && k < ts.length; k++) {
              c = ts.charAt(k);
              switch(c) {
              case 'Z':
                zinz += ' ';
                len++;
                break;
              case '9':
                zinz += '0';
                len++;
                break;
              case '.':
                if (zdot) {
                  zdot++;
                  zdone = true;
                  continue;
                }
                zinz += '.';
                len++;
                zdot++;
                break;
              default:
                break;
              }
            }
            // rpg free
            if (zdot == 1) {
              len--;
              zinz = zinz.substr(0,zinz.length-1);
            }
            len = len.toString();
            rpgfree += prefix + dataName + ' ' + rpg_d_s('a', len, "", null) + ";\n";
            isDone = true;
            break;
          //  77  COMMISSION PIC S99999V99 PACKED-DECIMAL VALUE 2000.00.
          //  77  WORK-DAYS PIC S9(4) BINARY VALUE 253
          //  15  TOTAL-PROJ-COST PIC S9(10)V99 PACKED-DECIMAL.
          case 'S':
            // PIC S9(n) DISPLAY                     zoned(n:0)
            p = tline.indexOf('DISPLAY');
            if (p > -1) {
              // rpg free
              len = rpg_free_len(ts);
              rpgfree += prefix + dataName + ' ' + rpg_d_s('s', len, "0", null) + ";\n";
              isDone = true;
              break;
            }
            // PIC S9(n-p)V9(p) COMP-3               decimal(n:p)
            // PIC S9(n-p)V9(p) PACKED-DECIMAL
            p = tline.indexOf('COMP-3');
            q = tline.indexOf('PACKED-DECIMAL');
            len = 0;
            scale = 0;
            if (p > -1 || q > -1) {
              var ss = ts.split('V');
              if (ss.length > 1) {
                var a9 = ss[0];
                p = a9.indexOf('(');
                if (p > -1) {
                  len = rpg_free_len(a9);
                } else {
                  for (k=0; k < a9.length; k++) {
                    c = a9.charAt(k);
                    if (c == '9') {
                      len++;
                    }
                  }
                }
                var a9 = ss[1];
                p = a9.indexOf('(');
                if (p > -1) {
                  scale = rpg_free_len(a9);
                } else {
                  for (k=0; k < a9.length; k++) {
                    c = a9.charAt(k);
                    if (c == '9') {
                      scale++;
                    }
                  }
                }
              }
              // rpg free
              len = len.toString();
              scale = scale.toString();
              rpgfree += prefix + dataName + ' ' + rpg_d_s('p', len, scale, null) + ";\n";
              isDone = true;
              break;
            }
            // PIC S9(4) COMP-4 BINARY               int(5)
            // PIC S9(4) COMP-4 with *NOSTDTRUNC
            // PIC S9(4) BINARY with *NOSTDTRUNC
            // PIC S9(4) COMP-5                      uns(5)
            p = tline.indexOf('S9(4)');
            if (p > -1) {
              // rpg free
              p = tline.indexOf('BINARY');
              q = tline.indexOf('COMP-4');
              if (p > -1 || q > -1) {
                rpgfree += prefix + dataName + ' ' + rpg_d_s('i', "5", "0", null) + ";\n";
              } else {
                rpgfree += prefix + dataName + ' ' + rpg_d_s('u', "5", "0", null) + ";\n";
              }
              isDone = true;
              break;
            }            
            // PIC S9(9) COMP-4                      int(10)
            // PIC S9(9) BINARY
            // PIC S9(9) COMP-4 with *NOSTDTRUNC
            // PIC S9(9) BINARY with *NOSTDTRUNC
            // PIC S9(9) COMP-5                      uns(10)
            p = tline.indexOf('S9(9)');
            if (p > -1) {
              // rpg free
              p = tline.indexOf('BINARY');
              q = tline.indexOf('COMP-4');
              if (p > -1 || q > -1) {
                rpgfree += prefix + dataName + ' ' + rpg_d_s('i', "10", "0", null) + ";\n";
              } else {
                rpgfree += prefix + dataName + ' ' + rpg_d_s('u', "10", "0", null) + ";\n";
              }
              isDone = true;
              break;
            }            
            // PIC S9(18) COMP-4                     int(20)
            // PIC S9(18) COMP-4 with *NOSTDTRUNC
            // PIC S9(18) BINARY with *NOSTDTRUNC
            // PIC S9(18) COMP-5                     uns(20)
            p = tline.indexOf('S9(18)');
            if (p > -1) {
              // rpg free
              p = tline.indexOf('BINARY');
              q = tline.indexOf('COMP-4');
              if (p > -1 || q > -1) {
                rpgfree += prefix + dataName + ' ' + rpg_d_s('i', "20", "0", null) + ";\n";
              } else {
                rpgfree += prefix + dataName + ' ' + rpg_d_s('u', "20", "0", null) + ";\n";
              }
              isDone = true;
              break;
            }            
            break;
          default:
            rpgfree += prefix + dataName + ' ' + rpg_d_s(ts, " ", " ", null) + ";\n";
            break;
          }
          break;
        } // if PIC
        lastName = s[j];
      } // j loop
    } else {
      isData = false;
    }
  } // i loop
  if (isDS) {
    rpgfree += "end-ds;\n";
  }
  isDS = false;
  // return cobolfree + "\n" + rpgfree;
  switch (rFlag) {
  case 1:
    return rpg_free_2_php_toolkit(rpgfree,false);
    break;
  case 2:
    return rpg_free_2_php_toolkit(rpgfree,true);
    break;
  default:
    break;
  }
  return rpgfree;
}
function cobol_sample() {
  var cobol = '';
  cobol +='       DATA DIVISION.' + "\n";
  cobol +='       FILE SECTION.' + "\n";
  cobol +='' + "\n";
  cobol +='       FD  PRINTFILE' + "\n";
  cobol +='           BLOCK CONTAINS 1 RECORDS' + "\n";
  cobol +='           LABEL RECORDS ARE OMITTED.' + "\n";
  cobol +='' + "\n";
  cobol +='       01  PRINT-RECORD PIC X(132).' + "\n";
  cobol +='' + "\n";
  cobol +='       WORKING-STORAGE SECTION.' + "\n";
  cobol +='       77  WORK-DAYS PIC S9(4) BINARY VALUE 253.' + "\n";
  cobol +='       77  RAISE-DATE PIC X(11) VALUE "1982-06-01".' + "\n";
  cobol +='       77  PERCENTAGE PIC S999V99 PACKED-DECIMAL.' + "\n";
  cobol +='       77  COMMISSION PIC S99999V99 PACKED-DECIMAL VALUE 2000.00.' + "\n";
  cobol +='' + "\n";
  cobol +='      ***************************************************************' + "\n";
  cobol +='      *  Structure for report 1.                                    *' + "\n";
  cobol +='      ***************************************************************' + "\n";
  cobol +='    1  01  RPT1.' + "\n";
  cobol +='           COPY DDS-PROJECT OF CORPDATA-PROJECT.' + "\n";
  cobol +='           05  EMPNO     PIC X(6).' + "\n";
  cobol +='           05  NAME      PIC X(30).' + "\n";
  cobol +='           05  SALARY    PIC S9(6)V99 PACKED-DECIMAL.' + "\n";
  cobol +='' + "\n";
  cobol +='      ***************************************************************' + "\n";
  cobol +='      *  Structure for report 2.                                    *' + "\n";
  cobol +='      ***************************************************************' + "\n";
  cobol +='       01  RPT2.' + "\n";
  cobol +='           15  PROJNO PIC X(6).' + "\n";
  cobol +='           15  PROJECT-NAME PIC X(36).' + "\n";
  cobol +='           15  EMPLOYEE-COUNT PIC S9(4) BINARY.' + "\n";
  cobol +='           15  TOTAL-PROJ-COST PIC S9(10)V99 PACKED-DECIMAL.' + "\n";
  cobol +='' + "\n";
  cobol +='      ***************************************************************' + "\n";
  cobol +='      *  Headers for reports.                                       *' + "\n";
  cobol +='      ***************************************************************' + "\n";
  cobol +='       01  RPT1-HEADERS.' + "\n";
  cobol +='           05  RPT1-HEADER1.' + "\n";
  cobol +='               10  FILLER PIC X(21) VALUE SPACES.' + "\n";
  cobol +='               10  FILLER PIC X(111)' + "\n";
  cobol +='                     VALUE "REPORT OF PROJECTS AFFECTED BY RAISES".' + "\n";
  cobol +='           05  RPT1-HEADER2.' + "\n";
  cobol +='               10  FILLER PIC X(9) VALUE "PROJECT".' + "\n";
  cobol +='               10  FILLER PIC X(10) VALUE "EMPID".' + "\n";
  cobol +='               10  FILLER PIC X(35) VALUE "EMPLOYEE NAME".' + "\n";
  cobol +='               10  FILLER PIC X(40) VALUE "SALARY".' + "\n";
  cobol +='       01  RPT2-HEADERS.' + "\n";
  cobol +='           05  RPT2-HEADER1.' + "\n";
  cobol +='               10  FILLER PIC X(21) VALUE SPACES.' + "\n";
  cobol +='               10  FILLER PIC X(111)' + "\n";
  cobol +='                       VALUE "ACCUMULATED STATISTICS BY PROJECT".' + "\n";
  cobol +='           05  RPT2-HEADER2.' + "\n";
  cobol +='               10  FILLER PIC X(9) VALUE "PROJECT".' + "\n";
  cobol +='               10  FILLER PIC X(38) VALUE SPACES.' + "\n";
  cobol +='               10  FILLER PIC X(16) VALUE "NUMBER OF".' + "\n";
  cobol +='               10  FILLER PIC X(10) VALUE "TOTAL".' + "\n";
  cobol +='           05  RPT2-HEADER3.' + "\n";
  cobol +='               10  FILLER PIC X(9) VALUE "NUMBER".' + "\n";
  cobol +='               10  FILLER PIC X(38) VALUE "PROJECT NAME".' + "\n";
  cobol +='               10  FILLER PIC X(16) VALUE "EMPLOYEES".' + "\n";
  cobol +='               10  FILLER PIC X(65) VALUE "COST".' + "\n";
  cobol +='       01  RPT1-DATA.' + "\n";
  cobol +='           05  PROJNO    PIC X(6).' + "\n";
  cobol +='           05  FILLER    PIC XXX VALUE SPACES.' + "\n";
  cobol +='           05  EMPNO     PIC X(6).' + "\n";
  cobol +='           05  FILLER    PIC X(4) VALUE SPACES.' + "\n";
  cobol +='           05  NAME      PIC X(30).' + "\n";
  cobol +='           05  FILLER    PIC X(3) VALUE SPACES.' + "\n";
  cobol +='           05  SALARY    PIC ZZZZZ9.99.' + "\n";
  cobol +='           05  FILLER    PIC X(96) VALUE SPACES.' + "\n";
  cobol +='       01  RPT2-DATA.' + "\n";
  cobol +='           05  PROJNO PIC X(6).' + "\n";
  cobol +='           05  FILLER PIC XXX VALUE SPACES.' + "\n";
  cobol +='           05  PROJECT-NAME PIC X(36).' + "\n";
  cobol +='           05  FILLER PIC X(4) VALUE SPACES.' + "\n";
  cobol +='           05  EMPLOYEE-COUNT PIC ZZZ9.' + "\n";
  cobol +='           05  FILLER PIC X(5) VALUE SPACES.' + "\n";
  cobol +='           05  TOTAL-PROJ-COST PIC ZZZZZZZZ9.99.' + "\n";
  cobol +='           05  FILLER PIC X(56) VALUE SPACES.' + "\n";
  return cobol;
}


// =========================
// html form (index.html)
// =========================
$(document).ready(function() {
  // form stay on browser (client)
  $('#myform').submit(function(e) {
    e.preventDefault();
    ile_var_clear();
    ile_const_clear();
    return false;
  });
  // clear
  $('#mybutton2').click(function() {
    $('#mytextarea1').val('');
    $('#mytextarea2').val('');
  });
  // rpg free
  $('#mybutton3').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var rpgfree = rpg_free_sample();
    var phptool = rpg_free_2_php_toolkit(rpgfree,false);
    $('#mytextarea1').val(rpgfree);
    $('#mytextarea2').val(phptool);
  });
  $('#mybutton4').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var rpgfree = $('#mytextarea1').val();
    var phptool = rpg_free_2_php_toolkit(rpgfree,false);
    $('#mytextarea2').val(phptool);
  });
  $('#mybutton5').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var rpgfree = $('#mytextarea1').val();
    var phptool = rpg_free_2_php_toolkit(rpgfree,true);
    $('#mytextarea2').val(phptool);
  });
  // rpg d spec
  $('#mybutton6').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var rpgdspec = rpg_dspec_sample();
    var phptool = rpg_d_2_php_toolkit(rpgdspec,1);
    $('#mytextarea1').val(rpgdspec);
    $('#mytextarea2').val(phptool);
  });
  $('#mybutton7').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var rpgdspec = $('#mytextarea1').val();
    var phptool = rpg_d_2_php_toolkit(rpgdspec,1);
    $('#mytextarea2').val(phptool);
  });
  $('#mybutton8').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var rpgdspec = $('#mytextarea1').val();
    var phptool = rpg_d_2_php_toolkit(rpgdspec,2);
    $('#mytextarea2').val(phptool);
  });
  $('#mybutton9').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var rpgdspec = $('#mytextarea1').val();
    var phptool = rpg_d_2_php_toolkit(rpgdspec,8);
    $('#mytextarea2').val(phptool);
  });
  // cobol
  $('#mybutton10').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var cobol = cobol_sample();
    var phptool = cobol_2_php_toolkit(cobol,1);
    $('#mytextarea1').val(cobol);
    $('#mytextarea2').val(phptool);
  });
  $('#mybutton11').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var cobol = $('#mytextarea1').val();
    var phptool = cobol_2_php_toolkit(cobol,1);
    $('#mytextarea1').val(cobol);
    $('#mytextarea2').val(phptool);
  });
  $('#mybutton12').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var cobol = $('#mytextarea1').val();
    var phptool = cobol_2_php_toolkit(cobol,2);
    $('#mytextarea1').val(cobol);
    $('#mytextarea2').val(phptool);
  });
  $('#mybutton13').click(function() {
    ILEPGM = $('#mypgm').val();
    ILELIB = $('#mylib').val();
    PHPFORMAT = $("#myradio1:checked").val();
    var cobol = $('#mytextarea1').val();
    var phptool = cobol_2_php_toolkit(cobol,99);
    $('#mytextarea1').val(cobol);
    $('#mytextarea2').val(phptool);
  });
});
